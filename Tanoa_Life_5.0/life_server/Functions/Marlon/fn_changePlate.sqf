/*
 author: Risk
 description: Ändert dein Kennzeichen in eines deiner Wünsche
 returns: nothing
*/
_vid = lbValue[2802,(lbCurSel 2802)];
_Buchstabe = ctrlText 2812;
_Zahl = ctrlText 2813;
_zuAendern = format["NN-%1-%2", _Buchstabe, _Zahl];
_length = count (toArray(_zuAendern));
_chrByte = toArray (_zuAendern);
_allowed = toArray("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-");
if (_length > 15) exitWith {hint "Du kannst das Kennzeichen nicht größer als 15 machen!";};
_badChar = false;
{
 if (!(_x in _allowed)) exitWith {
 _badChar = true;
 };
} forEach _chrByte;
if (_badChar) exitWith {hint localize "STR_GNOTF_IncorrectChar";};
_uid = getPlayerUID player;
closeDialog 0;
[_zuAendern,_uid,_vid] remoteExec ["TON_fnc_Plate",2];