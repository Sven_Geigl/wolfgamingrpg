_server = preprocessFile "\life_server\init.sqf"; 
_backend = preprocessFile "\ItsMarlon_BackEnd\Setup.sqf"; 

if (_server isEqualTo "" || _backend isEqualTo "") then { 
} else { 
    [] call compile _server;
    [] call compile _backend;
};