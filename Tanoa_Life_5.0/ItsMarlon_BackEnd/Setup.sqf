#include "\life_server\script_macros.hpp"
_itsMarlon_CString = {
    params["_in"];
    _out = "";
    if(typeName _in == "CODE") then {
        _str = str(_in);
        _out = _str select [1, (count(_str)-2)];
    };
    if(typeName _in == "STRING") then {
        _out = _in;
    };
    _out
};
itsMarlon_CString = compileFinal ([_itsMarlon_CString] call _itsMarlon_CString);

_itsMarlon_CFunction = {
    params["_in"];
    _code = compileFinal ([_in] call itsMarlon_CString);
    _code
};
itsMarlon_CFunction = [_itsMarlon_CFunction] call _itsMarlon_CFunction;

itsMarlon_ClientCompile = { 
  params [["_FuncName","",[""]],["_Function",{},[{}]],["_Fianl",true,[true]]];

  if (_Fianl isEqualTo false) then {
    _compile = format["%1 = %2;",_FuncName,_Function];
    call compile _compile;
  } else {
    _compile = format["%1 = [%2] call itsMarlon_CFunction;",_FuncName,_Function];
    call compile _compile;
  };

  diag_log format["Compiled Client Function %1",_FuncName];
  publicvariable _FuncName;
};

itsMarlon_ServerCompile = {
  params [["_FuncName","",[""]],["_Function",{},[{}]],["_Fianl",true,[true]]];

  if (_Fianl isEqualTo false) then {
    _compile = format["%1 = %2;",_FuncName,_Function];
    call compile _compile;
  } else {
    _compile = format["%1 = [%2] call itsMarlon_CFunction;",_FuncName,_Function];
    call compile _compile;
  };

  diag_log format["Compiled Server Function %1",_FuncName];
};

_funcs = [
  "\itsMarlon_Backend\Functions\Anticheat.sqf",
  "\itsMarlon_Backend\Functions\Init.sqf",
  "\itsMarlon_Backend\Functions\Civilian.sqf",
  "\itsMarlon_Backend\Functions\Gangs.sqf",
  "\itsMarlon_Backend\Functions\Misc.sqf",
  "\itsMarlon_Backend\Functions\Interaction.sqf",
  "\itsMarlon_Backend\Functions\Mining.sqf",
  "\itsMarlon_Backend\Functions\Housing.sqf",
  "\itsMarlon_Backend\Functions\Jobs.sqf",
  "\itsMarlon_Backend\Functions\Libary.sqf",
  "\itsMarlon_Backend\Functions\VIP.sqf",
  "\itsMarlon_Backend\Functions\EmergencyFactions.sqf",
  "\itsMarlon_Backend\Functions\EventHandlers.sqf",
  "\itsMarlon_Backend\Functions\Jail.sqf",
  "\itsMarlon_Backend\Functions\Police.sqf",
  "\itsMarlon_Backend\Functions\Loadout.sqf",
  "\itsMarlon_Backend\Functions\Transport.sqf"
];

{
  call compileFinal preprocessfile _x;
} foreach _funcs;

["itsMarlon_Nil",{
  itsMarlon_CString = nil;
  itsMarlon_CFunction = nil;
  itsMarlon_ClientCompile = nil;
  itsMarlon_ServerCompile = nil;
  itsMarlon_Nil = nil;
},false] call itsMarlon_ServerCompile;